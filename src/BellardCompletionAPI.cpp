#include "BellardCompletionAPI.hpp"

#include <string>
#include <string_view>
#include <sstream>
#include <array>
#include <memory>



void BellardCompletionAPI::complete(const std::string& prompt, char delim, std::function<void (const std::string&)> finally, std::function<void (const boost::beast::error_code&, std::string_view)> on_error) {
    using streamT = boost::beast::ssl_stream<boost::beast::tcp_stream>;
    auto stream = std::make_shared<streamT>(ex, ctx);

    // Set SNI Hostname (many hosts need this to handshake successfully)
    if (!SSL_set_tlsext_host_name(stream->native_handle(), "bellard.org")) {
        if (on_error) {
            on_error(boost::beast::error_code{static_cast<int>(::ERR_get_error()), boost::asio::error::get_ssl_category()}, "SSL setup");
        }
        return;
    }

    // Set up an HTTP GET request message
    auto req = std::make_shared<boost::beast::http::request<boost::beast::http::string_body>>();
    req->version(11);
    req->method(boost::beast::http::verb::post);
    req->target("/textsynth/api/v1/engines/gptj_6B/completions");
    req->set(boost::beast::http::field::host, "bellard.org");
    req->set(boost::beast::http::field::user_agent, BOOST_BEAST_VERSION_STRING);
    req->set(boost::beast::http::field::content_type, "application/json");
    req->body() = nlohmann::json({
                                     {"prompt", prompt},
                                     {"stream", true}
                                 }).dump();
    req->prepare_payload();

    // Look up the domain name
    resolver.async_resolve(
                "bellard.org",
                "443",
                [=]
                (boost::beast::error_code ec, boost::asio::ip::tcp::resolver::results_type results) {
        if (ec) {
            if (on_error) on_error(ec, "resolve");
            return;
        }

        // Make the connection on the IP address we get from a lookup
        boost::beast::get_lowest_layer(*stream).async_connect(
                    results,
                    [=]
                    (boost::beast::error_code ec, boost::asio::ip::tcp::resolver::results_type::endpoint_type) {
            if (ec) {
                if (on_error) on_error(ec, "connect");
                return;
            }

            // Perform the SSL handshake
            stream->async_handshake(
                        boost::asio::ssl::stream_base::client,
                        boost::beast::bind_front_handler(
                            [=, stream = std::move(stream), req = std::move(req)]
                            (boost::beast::error_code ec) {
                if (ec) {
                    if (on_error) on_error(ec, "SSL handshake");
                    return;
                }

                // Set a timeout on the stream
                boost::beast::get_lowest_layer(*stream).expires_after(timeout);

                // Send the HTTP request to the remote host
                boost::beast::http::async_write(*stream, *req,
                                                [=]
                                                (boost::beast::error_code ec, std::size_t) {
                    *req; // Forces capture of 'req'

                    if (ec) {
                        if (on_error) on_error(ec, "write");
                        return;
                    }

                    // Receive data
                    // We have to use shared pointers here since on_read is going to be called more than once
                    auto rawBuffer = std::make_shared<std::array<char, bufSize>>();
                    auto lineBuffer = std::make_shared<std::string>();
                    auto outputBuffer = std::make_shared<std::ostringstream>();
                    auto isFinished = std::make_shared<bool>(false);
                    using cbT = std::function<void (boost::beast::error_code, std::size_t)>;
                    auto on_read = std::make_shared<cbT>();
                    *on_read = [=]
                               (boost::system::error_code ec, std::size_t bytes_read) {
                        if (ec && ec != boost::asio::ssl::error::stream_truncated) {
                            if (on_error) on_error(ec, "read");
                            return;
                        }

                        if (bytes_read && !*isFinished) {
                            auto strBuffer = std::string_view{rawBuffer->data(), bytes_read};
                            for (const char c : strBuffer) {
                                if (c == '\r') {
                                    continue;
                                } else if (c == '\n') {
                                    // Process lineBuffer
                                    if (!lineBuffer->empty() && (*lineBuffer)[0] == '{') {
                                        try {
                                            auto jsonData = nlohmann::json::parse(std::move(*lineBuffer));
                                            if (jsonData.contains("error")) {
                                                finally(jsonData["error"]);
                                                *isFinished = true;
                                                break;
                                            }
                                            bool reachedEnd = jsonData["reached_end"];
                                            for (const char c : std::string(jsonData["text"])) {
                                                if (c == delim) {
                                                    reachedEnd = true;
                                                    break;
                                                } else {
                                                    *outputBuffer << c;
                                                }
                                            }
                                            if (reachedEnd) {
                                                finally(outputBuffer->str());
                                                *isFinished = true;
                                                break;
                                            }
                                        } catch (nlohmann::json::exception&) {
                                            // Ignore (for now)
                                        }
                                    }
                                    // Reset lineBuffer
                                    *lineBuffer = std::string();
                                } else {
                                    lineBuffer->push_back(c);
                                }
                            }
                        }

                        // Gracefully close the stream if EOF or read more data
                        if (ec == boost::asio::ssl::error::stream_truncated) {
                            stream->async_shutdown([] (...) {});
                        } else {
                            stream->async_read_some(boost::asio::mutable_buffer(rawBuffer->data(), rawBuffer->size()), *on_read);
                        }
                    };
                    (*on_read)(boost::beast::error_code(), 0);
                });
            }));
        });
    });
}
