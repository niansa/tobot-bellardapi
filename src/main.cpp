#include <iostream>
#include <string>
#include <vector>
#include "BellardCompletionAPI.hpp"

#include <string_view>
#include <functional>
#include <stdexcept>
#include <cstring>
#include <fmt/format.h>
#include <sys/wait.h>

struct BaseMsg {
    const char *lang_code, *start_msg, *msg;
};

std::vector<BaseMsg> baseMsgs = {
    {"en", " Hello!", R"(
<{0}> Hello.
<bot> Hey!
<{0}> What is your name?
<bot> My name is Tobot.
<{0}> And mine is {0}.
<bot>)"},
    {"de", " Hallo!", R"(
<{0}> Hallo.
<bot> Hey!
<{0}> Was ist dein Name?
<bot> Mein Name ist Tobot.
<{0}> Und meiner ist {0}.
<bot>)"}
};



void speak(const std::string& str, const char* language = "en", int pitch = 0) {
    if (fork() == 0) {
        execlp("spd-say", "spd-say", "-w", "-l", language, "-p", std::to_string(pitch).c_str(), "--", str.c_str(), nullptr);
    }
    wait(0);
}

int main(int argc, char **argv) {
    boost::asio::io_service io;

    // Get baseMsg
    auto baseMsg = baseMsgs.begin();
    if (argc > 1) {
        for (; ; baseMsg++) {
            if (baseMsg == baseMsgs.end()) {
                std::cerr << "Language not found: " << argv[1] << std::endl;
                return EXIT_FAILURE;
            }
            if (strcmp(baseMsg->lang_code, argv[1]) == 0) {
                break;
            }
        }
    }
    std::cout << "Language selected: " << baseMsg->lang_code << std::endl;

    // Get if text is to be spoken
    bool speakEnabled = getenv("DO_SPEAK");

    // Get username
    const char *username = getenv("USER");
    if (!username) {
        username = "user";
    }

    // Initlize completion
    BellardCompletionAPI completion(io);

    // Initialize chat history
    std::string history = fmt::format(baseMsg->msg, username);

    // Initialize callbacks
    auto errcb = [] (auto ec, auto where) {
        throw std::runtime_error("In "+std::string(where)+": "+ec.message());
    };
    std::function<void (const std::string& text)>
            cb = [&] (auto text) {
        // Print
        std::cout << "<bot>" << text << std::endl
                  << "<user> " << std::flush;
        if (speakEnabled) {
            // Speech output
            speak(text, baseMsg->lang_code, -100);
        }
        // Read next line
        std::string line;
        std::getline(std::cin, line);
        if (!line.empty()) {
            // Append to history
            history.append(fmt::format("{}\n<{}> {}\n<bot>", text, username, line));
            completion.complete(history, '\n', cb, errcb);
            if (speakEnabled) {
                // Speech output
                speak(line, baseMsg->lang_code);
            }
        } else {
            cb("");
        }
    };
    cb(baseMsg->start_msg);

    return io.run();
}
